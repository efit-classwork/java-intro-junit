package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LightSaberTest {
    @Test
    public void verifyChargeSetterSetsTheCharge(){
        LightSaber saber = new LightSaber(1234567890);
        saber.setCharge(50.0f);
        float test = saber.getCharge();
        assertEquals(50.0f,test);
    }
    @Test
    public void verifyChargeGetterReturnsTheSaberCharge(){
        LightSaber saber = new LightSaber(1234567890);
        float test = saber.getCharge();
        assertEquals(100.0f,test);
    }
    @Test
    public void verifyColorSetterOfLightSaberColorWork(){
        LightSaber saber = new LightSaber(1234567890);
        saber.setColor("purple");
        String test = saber.getColor();
        assertEquals("purple",test);
    }
    @Test
    public void verifyColorGetterOfLightSaberColorWork(){
        LightSaber saber = new LightSaber(1234567890);
        String test = saber.getColor();
        assertEquals("green",test);
    }
    @Test
    public void verifySaberSerialNumberGetterWorks(){
        LightSaber saber = new LightSaber(1234567890);
        long test = saber.getJediSerialNumber();
        assertEquals(1234567890, test);
    }
    @Test
    public void verifyThatUsingALightSaberChangesTheCharge(){
        LightSaber saber = new LightSaber(1234567890);
        saber.use(60.0f);
        float test = saber.getCharge();
        assertEquals(90.0f,test);
    }
    @Test
    public void verifyALightSaberCanBeCheckedForRemainingRunTime(){
        LightSaber saber = new LightSaber(1234567890);
        float test = saber.getRemainingMinutes();
        assertEquals(600.0f,test);
    }
    @Test
    public void verifyThatChargingALightSaberWorks(){
        LightSaber saber = new LightSaber(1234567890);
        saber.setCharge(0.0f);
        saber.recharge();
        float test = saber.getCharge();
        assertEquals(100.0f,test);
    }
}
